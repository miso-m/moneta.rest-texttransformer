package com.mizisin.moneta.service;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringTransformerTest {

	@Test
	public void shouldReturnEmptyString() {
		assertEquals(StringTransformer.transform(""), "");
	}

	@Test
	public void shouldReturnEmptyStringWhenNullProvided() {
		assertEquals(StringTransformer.transform(null), "");
	}

	@Test
	public void shouldReverseSimpleString() {
		String value = "bcd";
		String result = "dcb";

		assertEquals(StringTransformer.transform(value), result);
	}

	@Test
	public void shouldReverseAndCapitaliseString() {
		String value = "abc";
		String result = "cbA";

		assertEquals(StringTransformer.transform(value), result);
	}

	@Test
	public void shouldAcceptSpecialCharacters() {
		String value = "ab#c";
		String result = "c#bA";

		assertEquals(StringTransformer.transform(value), result);
	}

	@Test
	public void shouldTrimMultipleSpaces() {
		String value  = "a     bc";
		String result = "cb A";
		assertEquals(StringTransformer.transform(value), result);
	}

	@Test
	public void shouldTrimMultipleSpacesAtTheBeginning() {
		String value  = "   abc";
		String result = "cbA ";
		assertEquals(StringTransformer.transform(value), result);
	}

	@Test
	public void shouldTrimMultipleSpacesAtTheEnd() {
		String value  = "abc   ";
		String result = " cbA";
		assertEquals(StringTransformer.transform(value), result);
	}

	@Test
	public void shouldCapitalizeAllCharacters() {
		String value  = "aeiou";
		String result = "UOIEA";
		assertEquals(StringTransformer.transform(value), result);
	}

	@Test
	public void shouldLowercaseBandUppercaseA() {
		String value  = "Ba";
		String result = "Ab";
		assertEquals(StringTransformer.transform(value), result);
	}
}
