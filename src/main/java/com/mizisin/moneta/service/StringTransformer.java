package com.mizisin.moneta.service;

public class StringTransformer {

	static String EMPTY_STRING = "";

	public static String transform(String stringToTransform) {
		if (stringToTransform == null) {
			return EMPTY_STRING;
		}

		StringBuilder stringBuilder = new StringBuilder();
		char prevChar = '0';
		for (int i = stringToTransform.length() - 1; i >= 0; i--) {
			char currentChar = stringToTransform.charAt(i);
			if (prevChar == ' ' && currentChar == ' ') {
				continue;
			}

			stringBuilder.append(transformCharacter(currentChar));
			prevChar = currentChar;
		}
		return stringBuilder.toString();
	}

	private static char transformCharacter(char character) {
		if (character == 'a' || character == 'e' || character == 'i'
				|| character == 'o' || character == 'u') {
			return Character.toUpperCase(character);
		} else {
			return Character.toLowerCase(character);
		}
	}
}
