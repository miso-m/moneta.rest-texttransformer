package com.mizisin.moneta.controller;

import com.mizisin.moneta.service.StringTransformer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransformationController {

	@Value("${max.length:}")
	private int maxLength;

	@RequestMapping("/transform")
	@ResponseStatus(HttpStatus.OK)
	public String transformString(@RequestParam(value="value", defaultValue="") String stringToTransform) {
		if (stringToTransform.length() > maxLength) {
			throw new InvalidRequestException();
		}
		return StringTransformer.transform(stringToTransform);
	}
}
