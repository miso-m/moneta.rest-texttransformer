package com.mizisin.moneta.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Value is too long!")
public class InvalidRequestException extends RuntimeException {
}
