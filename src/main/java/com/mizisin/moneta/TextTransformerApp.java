package com.mizisin.moneta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TextTransformerApp {

	public static void main(String[] args) {
		SpringApplication.run(TextTransformerApp.class, args);
	}
}
