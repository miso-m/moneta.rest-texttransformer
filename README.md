# README #

### What is this repository for? ###

* RestTextTranformer for Moneta interview
* Napište program, který bude vystavovat REST službu, která na vstupu bude přijímat text. Služba text transformuje a vrátí zpět.

* Transformaci provede tak, že textu otočí pořadí písmen (tedy přečte ho odzadu) s tím, že:

> 1. na pozici, kde se původně vyskytovala písmena a,e,i,o,u budou nově písmena uppercase, všechna ostatní písmena budou lowercase.

> 2. dvě a více mezer spojí do jedné mezery


* SpringBoot aplikace s exposnutym jedinym endpointom transform, ktory prijima parameter value
* max.length v application.properties definuje maximalnu dlzku stringu ktory je transformovany

